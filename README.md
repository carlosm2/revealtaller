## Taller de revealJS

![flyer 2018](https://ranchoelectronico.org/wp-content/uploads/2018/01/reveal_enero_cacu.png)

Esto es reveal: [https://github.com/hakimel/reveal.js/](https://github.com/hakimel/reveal.js/)

Reveal en acción: [https://revealjs.com/](https://revealjs.com/)

## Estructura archivos

`/imagen/` -> aqui estan las imagenes de la presentación

`/css/theme/league.css` -> es el CSS que se esta usando en esta presentación.

`/masamigable.md` -> es el markdown externo usado en esta presentación.

`index.html` -> nuestro archivo maestro

## Licencia 

Licencia de producción de pares: [https://endefensadelsl.org/ppl_deed_es.html](https://endefensadelsl.org/ppl_deed_es.html)

## Nota 2023: algunas cosas de esta presentación siguen siendo utiles pero han habido muchos cambios en revealjs

